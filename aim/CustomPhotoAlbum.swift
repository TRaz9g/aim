//
//  CustomPhotoAlbum.swift
//  aim
//
//  Created by Артем Селюжицкий on 28.07.15.
//  Copyright (c) 2015 traz. All rights reserved.
//

import Photos

class CustomPhotoAlbum {
    
    static let albumName = "Aim"
    static let sharedInstance = CustomPhotoAlbum()
    
    var assetCollection: PHAssetCollection!
    
    init() {
        
        func fetchAssetCollectionForAlbum() -> PHAssetCollection! {
            
            let fetchOptions = PHFetchOptions()
            fetchOptions.predicate = NSPredicate(format: "title = %@", CustomPhotoAlbum.albumName)
            let collection = PHAssetCollection.fetchAssetCollectionsWithType(.Album, subtype: .Any, options: fetchOptions)
            
            if let firstObject: AnyObject = collection.firstObject {
                return firstObject as! PHAssetCollection
            }
            
            return nil
        }
        
        if let assetCollection = fetchAssetCollectionForAlbum() {
            self.assetCollection = assetCollection
            return
        }
        
        PHPhotoLibrary.sharedPhotoLibrary().performChanges({
            PHAssetCollectionChangeRequest.creationRequestForAssetCollectionWithTitle(CustomPhotoAlbum.albumName)
            }) { success, _ in
                if success {
                    self.assetCollection = fetchAssetCollectionForAlbum()
                }
        }
    }
    
    func saveImage(image: UIImage) {
        if assetCollection == nil {
            return   // If there was an error upstream, skip the save.
        }
        
//        PHPhotoLibrary.sharedPhotoLibrary().performChanges({
//            let assetChangeRequest = PHAssetChangeRequest.creationRequestForAssetFromImage(image)
//            let assetPlaceholder = assetChangeRequest.placeholderForCreatedAsset as! NSFastEnumeration
//            let albumChangeRequest = PHAssetCollectionChangeRequest(forAssetCollection: self.assetCollection)
//            albumChangeRequest?.addAssets([assetPlaceholder])
//            }, completionHandler: nil)
    }
}