//
//  CVWrapper.h
//  aim
//
//  Created by Артем Селюжицкий on 05.09.15.
//  Copyright (c) 2015 traz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CVWrapper : NSObject


+ (UIImage*) imageCounture: (UIImage*) inputImage;

+ (UIImage*) processImageWithOpenCV: (UIImage*) inputImage;

+ (UIImage*) processWithOpenCVImage1:(UIImage*)inputImage1 image2:(UIImage*)inputImage2;

+ (UIImage*) processWithArray:(NSArray*)imageArray;


@end