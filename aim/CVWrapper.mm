#import "CVWrapper.h"
#import "UIImage+OpenCV.h"
#import "stitching.h"



//////////////
using namespace cv;
using namespace std;

Mat src; Mat src_gray;
int thresh = 100;
int max_thresh = 255;
RNG rng(12345);

/// Function header
void thresh_callback(int, void* );
//////////////


@implementation CVWrapper


+ (UIImage*) imageCounture: (UIImage*) inputImage
{
    cv::Mat matImage = [inputImage CVMat3];
    
    /// Convert image to gray and blur it
    cvtColor( matImage, src_gray, CV_BGR2GRAY );
    blur( src_gray, src_gray, cv::Size(3, 3) );
    
    //////////////////
    vector<vector<cv::Point> > contours;
    vector<Vec4i> hierarchy;
    
    /// Detect edges using canny
    Canny( src_gray, matImage, thresh, thresh*2, 3 );
    /// Find contours
    findContours( matImage, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0) );
    
    /// Draw contours
    Mat drawing = Mat::zeros( matImage.size(), CV_8UC3 );
    
    Scalar color = Scalar( 200, 200, 200 );
    for( int i = 0; i< contours.size(); i++ )
    {
        
        drawContours( drawing, contours, i, color, 1, 8, hierarchy, 0, cv::Point() );
    }
    ///////////////////
    
    
    UIImage* result =  [UIImage imageWithCVMat:drawing];
    
    
    return result;
}





+ (UIImage*) processImageWithOpenCV: (UIImage*) inputImage
{
    NSArray* imageArray = [NSArray arrayWithObject:inputImage];
    UIImage* result = [[self class] processWithArray:imageArray];
    return result;
}

+ (UIImage*) processWithOpenCVImage1:(UIImage*)inputImage1 image2:(UIImage*)inputImage2;
{
    NSArray* imageArray = [NSArray arrayWithObjects:inputImage1,inputImage2,nil];
    UIImage* result = [[self class] processWithArray:imageArray];
    return result;
}

+ (UIImage*) processWithArray:(NSArray*)imageArray
{
    if ([imageArray count]==0){
        NSLog (@"imageArray is empty");
        return 0;
    }
    std::vector<cv::Mat> matImages;
    
    for (id image in imageArray) {
        if ([image isKindOfClass: [UIImage class]]) {
            cv::Mat matImage = [image CVMat3];
            NSLog (@"matImage: %@",image);
            matImages.push_back(matImage);
        }
    }
    NSLog (@"stitching...");
    cv::Mat stitchedMat = stitch (matImages);
    UIImage* result =  [UIImage imageWithCVMat:stitchedMat];
    return result;
}


@end