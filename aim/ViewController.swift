//
//  ViewController.swift
//  fghjk
//
//  Created by Артем Селюжицкий on 11.07.15.
//  Copyright (c) 2015 traz. All rights reserved.
//

import UIKit
import AVFoundation
import Foundation
import MediaPlayer

@objc protocol CameraSessionControllerDelegate {
    optional func cameraSessionDidOutputSampleBuffer(sampleBuffer: CMSampleBuffer!)
}

class ViewController: UIViewController, NSStreamDelegate, AVCaptureVideoDataOutputSampleBufferDelegate {
    let MENU_STATE_MAIN = "state_main"
    let MENU_STATE_DISPLAY_MODE = "state_disaplay_mode"
    let MENU_STATE_DATA = "state_data"
    let MENU_STATE_RECORDING = "state_recording"
    let MENU_STATE_FUSION = "state_fusion"
    let MENU_STATE_COM = "state_com"
    
    let defualtTimeToHideContent = 2 + 1
    var menuLastState = ""
    
    var hideControlsTimer = NSTimer()
    var secondToHideAllControls = 0
    
    @IBOutlet weak var recortControlsLayer: UIView!
    
    var cameraMode = "photo"
    
    var cameraPalette = AimFrameCreator.GREY_PALLETE
    
    //Camerea variables
    let captureSession = AVCaptureSession()
    var previewLayer : AVCaptureVideoPreviewLayer?
    let stillImageOutput = AVCaptureStillImageOutput()
    
    // If we find a device we'll store it here for later use
    var captureDevice : AVCaptureDevice?
    
    @IBOutlet weak var optionsLayerView: UIView!
    @IBOutlet weak var videoTimeLayer: UIView!
    @IBOutlet weak var compassIndicatorLayer: UIView!
    @IBOutlet weak var menuItemLayer: UIView!
    
    @IBOutlet weak var menuItemDisplayMode: UIView!
    @IBOutlet weak var menuItemData: UIView!
    @IBOutlet weak var menuItemRecording: UIView!
    
    @IBOutlet weak var switchCameraModeView: UIImageView!
    @IBOutlet weak var shotButonView: UIImageView!
    @IBOutlet weak var cameraRedCircleView: UIImageView!
    @IBOutlet weak var compassLineView: UIImageView!
    @IBOutlet weak var fakeCameraView: UIImageView!
    @IBOutlet weak var cameraViewBackgroundRepeater: UIImageView!
    
    @IBOutlet weak var cameraTimerView: UILabel!
    @IBOutlet weak var compassDegreeView: UILabel!
    @IBOutlet weak var menuItemLabel: UILabel!
    
    //Main layout view
    @IBOutlet var mainLayout: UIView!
    
    var videoRecordTimer = NSTimer()
    var fakeCompassTimer = NSTimer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.optionsLayerView.hidden = true
        self.videoTimeLayer.hidden = true
        self.cameraRedCircleView.hidden = true
        
        self.menuItemDisplayMode.hidden = true
        self.menuItemData.hidden = true
        self.menuItemRecording.hidden = true
        
        // Trying to update module programm
        //downloadNewModuleProgramm()
        
        //resizeCameraView()
        
        maskCompassLayer()
        setupFakeCompassTimer()
        
        //self.videoTestArrayFunc()
        
        self.runSockets()
        
        //Запускаем таймер на скрытие кнопок
        secondToHideAllControls = self.defualtTimeToHideContent
        runHideControlsTimer();
        
        //createVideo()
        
        
        //Запрет перехода в режим сна
        UIApplication.sharedApplication().idleTimerDisabled = true
        
        // Do any additional setup after loading the view, typically from a nib.
        captureSession.sessionPreset = AVCaptureSessionPresetHigh
        
        let devices = AVCaptureDevice.devices()
        
        // Loop through all the capture devices on this phone
        for device in devices {
            // Make sure this particular device supports video
            if (device.hasMediaType(AVMediaTypeVideo)) {
                // Finally check the position and confirm we've got the back camera
                if(device.position == AVCaptureDevicePosition.Back) {
                    captureDevice = device as? AVCaptureDevice
                    
                    if captureDevice != nil {
                        print("Capture device found")
                        //beginSession()
                        
                        initializeAndStartSessionWithoutCaptureSound_1()
                    }
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
    }
    
    @IBAction func showAllControls(sender: AnyObject) {
        secondToHideAllControls = defualtTimeToHideContent
        
        if (menuLastState == MENU_STATE_MAIN) {
            self.optionsLayerView.hidden = false
            self.optionsLayerView.alpha = 1.0
            
            videoTimeLayer.alpha = 0.3
        } else if (menuLastState == MENU_STATE_DISPLAY_MODE) {
            dysplayModeMenuAction(sender)
        } else if (menuLastState == MENU_STATE_DATA) {
            dataMenuAction(sender)
        } else if (menuLastState == MENU_STATE_RECORDING) {
            recordingMenuAction(sender)
        } else if (menuLastState == MENU_STATE_FUSION) {
            fusionMenuAction(sender)
        } else if (menuLastState == MENU_STATE_COM) {
            comMenuAction(sender)
        }
        
        if (optionsLayerView.hidden == false || menuItemLayer.hidden == false) {
            optionsLayerView.hidden = true
            menuItemLayer.hidden = true
            
            self.menuItemDisplayMode.hidden = true
            self.menuItemData.hidden = true
            self.menuItemRecording.hidden = true
            
            videoTimeLayer.alpha = 1
            
            menuLastState = ""
        }
        
        recortControlsLayer.hidden = false
        UIView.animateWithDuration(0.3, animations: {
            self.recortControlsLayer.alpha = 1
            }, completion: {
                (value: Bool) in
                
        })
    }
    
    func runHideControlsTimer() {
        hideControlsTimer = NSTimer.scheduledTimerWithTimeInterval(1, target:self, selector: Selector("hideControlsTimerTask"), userInfo: nil, repeats: true)
    }
    
    func hideControlsTimerTask() {
        if (secondToHideAllControls == 1) {
            self.recortControlsLayer.hidden = true
            
            self.optionsLayerView.hidden = true
            self.menuItemLayer.hidden = true
        } else if (secondToHideAllControls >= 1) {
            secondToHideAllControls--
        }
    }
    
    @IBAction func dysplayModeMenuAction(sender: AnyObject) {
        secondToHideAllControls = defualtTimeToHideContent
        
        menuItemLabel.text = "Display Mode"
        
        self.optionsLayerView.hidden = true
        self.menuItemLayer.hidden = false
        
        self.menuItemDisplayMode.hidden = false
        
        menuLastState = MENU_STATE_DISPLAY_MODE
    }
    
    @IBAction func dataMenuAction(sender: AnyObject) {
        secondToHideAllControls = defualtTimeToHideContent
        
        menuItemLabel.text = "Data"
        
        self.optionsLayerView.hidden = true
        self.menuItemLayer.hidden = false
        
        self.menuItemData.hidden = false
        
        menuLastState = MENU_STATE_DATA
    }
    
    @IBAction func recordingMenuAction(sender: AnyObject) {
        secondToHideAllControls = defualtTimeToHideContent
        
        menuItemLabel.text = "Recording"
        
        self.optionsLayerView.hidden = true
        self.menuItemLayer.hidden = false
        
        self.menuItemRecording.hidden = false
        
        menuLastState = MENU_STATE_RECORDING
    }
    
    @IBAction func fusionMenuAction(sender: AnyObject) {
        secondToHideAllControls = defualtTimeToHideContent
        
        self.optionsLayerView.hidden = true
        self.menuItemLayer.hidden = false
        
        menuItemLabel.text = "Fusion"
        
        menuLastState = MENU_STATE_FUSION
    }
    
    @IBAction func comMenuAction(sender: AnyObject) {
        secondToHideAllControls = defualtTimeToHideContent
        
        self.optionsLayerView.hidden = true
        self.menuItemLayer.hidden = false
        
        menuItemLabel.text = "Com"
        
        menuLastState = MENU_STATE_COM
    }
    
    @IBAction func backToMenuAction(sender: AnyObject) {
        secondToHideAllControls = defualtTimeToHideContent
        
        menuLastState = MENU_STATE_MAIN
        
        self.optionsLayerView.hidden = false
        self.menuItemLayer.hidden = true
        
        self.menuItemDisplayMode.hidden = true
        self.menuItemData.hidden = true
        self.menuItemRecording.hidden = true
    }
    
    func maskCompassLayer() {
        let maskingImage = UIImage(named: "mask_compass-indicator.png")
        let maskingLayer = CALayer()
        
        maskingLayer.frame = compassIndicatorLayer.bounds
        maskingLayer.contents = maskingImage?.CGImage
        compassIndicatorLayer.layer.mask = maskingLayer
    }
    
    @IBAction func someAction(sender: AnyObject) {
        let alert = UIAlertController(title: "Alert", message: "Message", preferredStyle:UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func showMenuOptions(sender: AnyObject) {
        if (menuLastState != "" && secondToHideAllControls > 1) {
            secondToHideAllControls = defualtTimeToHideContent
            
            optionsLayerView.hidden = true
            menuItemLayer.hidden = true
            
            self.menuItemDisplayMode.hidden = true
            self.menuItemData.hidden = true
            self.menuItemRecording.hidden = true
            
            
            videoTimeLayer.alpha = 1
            
            menuLastState = ""
            
            return
        }

        secondToHideAllControls = defualtTimeToHideContent
        showAllControls(sender)
        
        
        if (self.menuItemLayer.hidden == false) {
            return
        }
        
        if (menuLastState == MENU_STATE_MAIN) {
            return
        }
        
        if (self.optionsLayerView.hidden) {
            menuLastState = MENU_STATE_MAIN
            
            self.optionsLayerView.hidden = false
            videoTimeLayer.alpha = 0.3
            
            UIView.animateWithDuration(0.3, animations: {
                self.optionsLayerView.alpha = 1.0
            })
        } else {
            videoTimeLayer.alpha = 1
            
            UIView.animateWithDuration(0.3, animations: {
                self.optionsLayerView.alpha = 0
                }, completion: {
                    (value: Bool) in
                    self.optionsLayerView.hidden = true
                    
                    self.menuLastState = ""
            })
        }
    }
    
    @IBOutlet weak var greyRadioButton: UIImageView!
    @IBOutlet weak var greyTextLabel: UILabel!
    @IBAction func videoGreyModeAction(sender: AnyObject) {
        secondToHideAllControls = defualtTimeToHideContent
        
        cameraPalette = AimFrameCreator.GREY_PALLETE
        
        greyRadioButton!.image = UIImage(named: "Radio-btn_active")
        greyTextLabel.alpha = 1.0
        
        invertedGreyButton!.image = UIImage(named: "Radio-btn_normal")
        invertedGreyTextLabel.alpha = 0.6
        colorRadioButton!.image = UIImage(named: "Radio-btn_normal")
        colorTextLabel.alpha = 0.6
        fusionRadioButton!.image = UIImage(named: "Radio-btn_normal")
        fusionTextLabel.alpha = 0.6
    }
    
    @IBOutlet weak var invertedGreyButton: UIImageView!
    @IBOutlet weak var invertedGreyTextLabel: UILabel!
    @IBAction func videoInvertedGreyModeAction(sender: AnyObject) {
        secondToHideAllControls = defualtTimeToHideContent
        
        cameraPalette = AimFrameCreator.INVERTED_GREY_PALLETE
        
        invertedGreyButton!.image = UIImage(named: "Radio-btn_active")
        invertedGreyTextLabel.alpha = 1.0
        
        greyRadioButton!.image = UIImage(named: "Radio-btn_normal")
        greyTextLabel.alpha = 0.6
        colorRadioButton!.image = UIImage(named: "Radio-btn_normal")
        colorTextLabel.alpha = 0.6
        fusionRadioButton!.image = UIImage(named: "Radio-btn_normal")
        fusionTextLabel.alpha = 0.6
    }
    
    @IBOutlet weak var colorRadioButton: UIImageView!
    @IBOutlet weak var colorTextLabel: UILabel!
    @IBAction func videoColorModeAction(sender: AnyObject) {
        secondToHideAllControls = defualtTimeToHideContent
        
        cameraPalette = AimFrameCreator.COLOR_PALLETE
        
        colorRadioButton!.image = UIImage(named: "Radio-btn_active")
        colorTextLabel.alpha = 1.0
        
        greyRadioButton!.image = UIImage(named: "Radio-btn_normal")
        greyTextLabel.alpha = 0.6
        invertedGreyButton!.image = UIImage(named: "Radio-btn_normal")
        invertedGreyTextLabel.alpha = 0.6
        fusionRadioButton!.image = UIImage(named: "Radio-btn_normal")
        fusionTextLabel.alpha = 0.6
    }
    
    @IBOutlet weak var fusionRadioButton: UIImageView!
    @IBOutlet weak var fusionTextLabel: UILabel!
    @IBAction func videoFusionModeAction(sender: AnyObject) {
        secondToHideAllControls = defualtTimeToHideContent
        
        cameraPalette = AimFrameCreator.FUSION_PALLETE
        
        
        fusionRadioButton!.image = UIImage(named: "Radio-btn_active")
        fusionTextLabel.alpha = 1.0
        
        
        greyRadioButton!.image = UIImage(named: "Radio-btn_normal")
        greyTextLabel.alpha = 0.6
        invertedGreyButton!.image = UIImage(named: "Radio-btn_normal")
        invertedGreyTextLabel.alpha = 0.6
        colorRadioButton!.image = UIImage(named: "Radio-btn_normal")
        colorTextLabel.alpha = 0.6
    }
    
    //Data actions
    @IBOutlet weak var clockLabel: UILabel!
    @IBOutlet weak var clockRadioButton: UIImageView!
    @IBOutlet weak var menuClockLabel: UILabel!
    @IBAction func showHideClockAction(sender: AnyObject) {
        secondToHideAllControls = defualtTimeToHideContent
        
        if (clockLabel.hidden == true) {
            clockLabel.hidden = false
            
            clockRadioButton.image = UIImage(named: "Radio-btn_active")
            menuClockLabel.alpha = 1.0
        } else {
            clockLabel.hidden = true
            
            clockRadioButton.image = UIImage(named: "Radio-btn_normal")
            menuClockLabel.alpha = 0.6
        }
    }
    
    @IBOutlet weak var compassRadioButton: UIImageView!
    @IBOutlet weak var menuCompassLabel: UILabel!
    @IBAction func showHideCompassAction(sender: AnyObject) {
        secondToHideAllControls = defualtTimeToHideContent
        
        if (compassIndicatorLayer.hidden == true) {
            compassIndicatorLayer.hidden = false
            compassDegreeView.hidden = false
            
            compassRadioButton.image = UIImage(named: "Radio-btn_active")
            menuCompassLabel.alpha = 1.0
        } else {
            compassIndicatorLayer.hidden = true
            compassDegreeView.hidden = true
            
            compassRadioButton.image = UIImage(named: "Radio-btn_normal")
            menuCompassLabel.alpha = 0.6
        }
    }
    
    @IBAction func switchCameraMode(sender: AnyObject) {
        secondToHideAllControls = defualtTimeToHideContent
        
        if (self.cameraMode == "photo") {
            self.cameraMode = "video"
            
            changeVideoTimeVisibility(true)
            changeShotButtonImage("video")
            
            switchCameraModeView!.alpha = 0.2
            switchCameraModeView!.image = UIImage(named: "btn_photo-mode.png");
            
            UIView.animateWithDuration(1.0, delay: 0, options:UIViewAnimationOptions.CurveEaseOut, animations: {
                self.switchCameraModeView!.alpha = 1.0;
                },
                completion: {(Bool) in
            })
        } else if (self.cameraMode == "video") {
            self.cameraMode = "photo"
            
            changeVideoTimeVisibility(false)
            changeShotButtonImage("photo")
            
            switchCameraModeView!.alpha = 0.2
            switchCameraModeView!.image = UIImage(named: "btn_video-mode.png");
            
            UIView.animateWithDuration(1.0, delay: 0, options:UIViewAnimationOptions.CurveEaseOut, animations: {
                self.switchCameraModeView!.alpha = 1.0;
                },
                completion: {(Bool) in
            })
        }
    }
    
    func changeVideoTimeVisibility(visibilyty: Bool) {
        self.videoTimeLayer.hidden = !visibilyty;
    }
    
    func changeShotButtonImage(state: String) {
        if (state == "photo") {
            self.shotButonView!.image = UIImage(named: "btn_take-photo")
        } else if (state == "video") {
            self.shotButonView!.image = UIImage(named: "btn_take-video")
        }
    }
    
    //Обработка нажатия кнопки старта видеозаписи
    @IBAction func shotButtonAction(sender: AnyObject) {
        secondToHideAllControls = defualtTimeToHideContent
        
        if (self.cameraMode == "photo") {
            //Сохраняем фото с тепловизора
            self.saveImageToGallery()
            
            //Анимируем кнопку
            shotButonView!.alpha = 0.2
            shotButonView!.image = UIImage(named: "btn_take-photo.png");
            
            UIView.animateWithDuration(1.0, delay: 0, options:UIViewAnimationOptions.CurveEaseOut, animations: {
                self.shotButonView!.alpha = 1.0;
                },
                completion: {(Bool) in
            })
            
        } else if (self.cameraMode == "video") {
            //Меняем состояние на запись видео. Анимируем кнопку старта видеозаписи и заменяем изображение кнопки на изображение остановки записи. Запускаем таймер
            
            self.cameraMode = "record_video"
            self.cameraRedCircleView.hidden = false;
            
            self.startRecordTimer()
            
            shotButonView!.alpha = 0.2
            shotButonView!.image = UIImage(named: "btn_stop-video.png");
            
            UIView.animateWithDuration(1.0, delay: 0, options:UIViewAnimationOptions.CurveEaseOut, animations: {
                self.shotButonView!.alpha = 1.0;
                },
                completion: {(Bool) in
            })
            
            //Возращаем в исходное состояние. Останавливаем таймер
        } else if (self.cameraMode == "record_video") {
            self.cameraMode = "video"
            
            self.cameraRedCircleView.hidden = true
            
            self.stopRecordTimer()
            
            self.shotButonView!.alpha = 0.2
            self.shotButonView!.image = UIImage(named: "btn_take-video.png");
            
            UIView.animateWithDuration(1.0, delay: 0, options:UIViewAnimationOptions.CurveEaseOut, animations: {
                self.shotButonView!.alpha = 1.0;
                },
                completion: {(Bool) in
            })
        }
    }
    
    
    var videoFramesFromView: NSMutableArray = []
    
    func startRecordTimer() {
        self.videoFramesFromView = []
        
        self.videoRecordTimer = NSTimer.scheduledTimerWithTimeInterval(0.1, target:self, selector: Selector("timerTask"), userInfo: nil, repeats: true)
    }
    
    func stopRecordTimer() {
        self.videoRecordTimer.invalidate()
        self.counter = 0;
        
        self.cameraTimerView.text = "00:00:00"
        
        let set = CEMovieMaker.videoSettingsWithCodec(AVVideoCodecH264, withWidth: 192, andHeight: 240)
        let movieMaker = CEMovieMaker(settings: set)
        
        movieMaker.createMovieFromImages(videoFramesFromView.copy() as! [AnyObject], withCompletion: { (fileURL) -> Void in
            self.saveToCameraRoll(fileURL)
        })
    }
    
    var counter = 0;
    
    func timerTask() {
        let size = CGSize(width: 192, height: 240)
        
        self.videoFramesFromView.addObject(RBResizeImage(fakeCameraView.image!, targetSize: size))
        
        self.counter++
        
        if ((counter % 10) == 0) {
            let timeCounter = counter / 10
            
            let hours = timeCounter / 3600
            let minutes = (timeCounter / 60) % 60
            let seconds = timeCounter % 60
            
            cameraTimerView.text = timerNumberFormat(hours) + ":" + timerNumberFormat(minutes) + ":" + timerNumberFormat(seconds)
        }
    }
    
    func timerNumberFormat(number: Int) -> String {
        if (number < 10) {
            return "0" + String(number)
        } else {
            return String (number)
        }
    }
    
    func setupFakeCompassTimer() {
        fakeCompassTimer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("fakeCompassTask"), userInfo: nil, repeats: true)
    }
    
    var compassDegreeCounter = 0;
    
    func fakeCompassTask() {
        compassDegreeCounter++
        
        if (compassDegreeCounter > 360) {
            compassDegreeCounter = 0;
        }
        
        self.compassDegreeView.text = String(compassDegreeCounter) + "°SW"
        self.moveCompassLineView(compassDegreeCounter)
    }
    
    func moveCompassLineView(position: Int) {
        var newFrame = self.compassLineView.frame
        newFrame.origin.x = self.calculatePosition(position)
        self.compassLineView.frame = newFrame;
    }
    
    func calculatePosition(degree: Int) -> CGFloat {
        let startPosition:Double = 99.0
        let oneDegree = 3.5
        
        let sum = Double(degree) * oneDegree
        
        let result = startPosition + sum
        
        return CGFloat(result * -1.0);
    }
    
    func getImageFromRaw(rawDataArray: [UInt8]) -> UIImage {
        let rawData = NSData(bytes: rawDataArray as [UInt8], length: rawDataArray.count)
        let image: UIImage = UIImage(data: rawData)!
        
        return image;
    }
    
    func imageFromARGB32Bitmap(pixels: [FrameStructure], width: Int, height: Int) -> UIImage? {
        let bitsPerComponent: Int = 8
        let bitsPerPixel: Int = 32
        let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo:CGBitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.PremultipliedFirst.rawValue)
        
        var data = pixels
        
        let providerRef = CGDataProviderCreateWithCFData(NSData(bytes: &data, length: data.count * sizeof(FrameStructure)))
        
        let cgim = CGImageCreate(
            width,
            height,
            bitsPerComponent,
            bitsPerPixel,
            width * Int(sizeof(FrameStructure)),
            rgbColorSpace,
            bitmapInfo,
            providerRef,
            nil,
            true,
            CGColorRenderingIntent.RenderingIntentDefault
        )
        
        return UIImage(CGImage: cgim!)
    }
    
    var videoTimer = NSTimer();
    var videoTestArray = [UInt16]();
    
    func videoTestArrayFunc() {
        let filePath = NSBundle.mainBundle().pathForResource("rawVideoExample", ofType:"rwe")
        let data     = NSData(contentsOfFile:filePath!)
        let dataRange = NSRange(location: 0, length: data!.length)
        
        videoTestArray = [UInt16](count: data!.length, repeatedValue: 0)
        data!.getBytes(&videoTestArray, range: dataRange)
        
        self.runVideo(videoTestArray);
    }
    
    func runVideo(videoArray:[UInt16]) {
        videoTimer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("fakeVideoTask"), userInfo: nil, repeats: true)
    }
    
    var currenFrameNumber = Int()
    
    func fakeVideoTask() {
        let framesCount:Int = self.videoTestArray.count / 4800 - 1
        
        if (currenFrameNumber >= framesCount / 2) {
            currenFrameNumber = 0
        }
        
        let data = halfRood(self.videoTestArray, frame: self.currenFrameNumber)
        
        if let image = imageFromARGB32Bitmap(data, width: Int(80), height: Int(60)) {
            var portraitImage = UIImage()
            
            if (UIDevice.currentDevice().orientation == UIDeviceOrientation.Portrait) {
                portraitImage = UIImage(CGImage: image.CGImage!,
                    scale: 1.0 ,
                    orientation: UIImageOrientation.Right)
            } else {
                portraitImage = UIImage(CGImage: image.CGImage!,
                    scale: 1.0 ,
                    orientation: UIImageOrientation.Up)
            }
            
            fakeCameraView.image = portraitImage
            cameraViewBackgroundRepeater.image = portraitImage
            
//            if (cameraPalette == AimFrameCreator.FUSION_PALLETE) {
//                self.imagePreview.image = self.lastCameraFrameWithCountur
//            } else {
//                self.imagePreview.image = UIImage()
//            }
        }
        
        currenFrameNumber++
    }
    
    func halfRood(videoArray:[UInt16], frame: Int)->[FrameStructure] {
        var data: [FrameStructure] = []
        let startPosition = frame * 160 * 120
        
        var tempVideoArray:[UInt16] = [UInt16](count: 160 * 120, repeatedValue: 0)
        
        for frame in 0 ..< 160 * 120 {
            tempVideoArray[frame] = videoArray[startPosition + frame]
        }
        
        let finalVideoFrame : [UInt8] = transformArray(tempVideoArray)
        
        for (var y = 0; y < 160 * 120; y++) {
            data.append(AimFrameCreator.createPixelData(Int(finalVideoFrame[y]), framePalleteType: cameraPalette))
        }
        
        return data
    }
    
    func setImageFromRawFrame(newFrameRawArray:[UInt16]) {
        let data = getDataFromFrame(newFrameRawArray)
        
        if let image = imageFromARGB32Bitmap(data, width: Int(160), height: Int(120)) {
            
            let portraitImage: UIImage
            
            if (UIDevice.currentDevice().orientation == UIDeviceOrientation.Portrait) {
                portraitImage = UIImage(CGImage: image.CGImage!,
                    scale: 1.0 ,
                    orientation: UIImageOrientation.Right)
            } else {
                portraitImage = UIImage(CGImage: image.CGImage!,
                    scale: 1.0 ,
                    orientation: UIImageOrientation.Up)
            }
            
            fakeCameraView.image = portraitImage
            cameraViewBackgroundRepeater.image = portraitImage
        }
    }
    
    func getDataFromFrame(tempVideoArray:[UInt16])->[FrameStructure] {
        var data: [FrameStructure] = []
        let finalVideoFrame : [UInt8] = transformArray(tempVideoArray)
        
        for y in 0 ..< 160 * 120 {
            data.append(AimFrameCreator.createPixelData(Int(finalVideoFrame[y]), framePalleteType: cameraPalette))
        }
        
        return data
    }
    
    func transformArray(rawVideoArray: [UInt16]) -> [UInt8] {
        var min:Int = 1 << 15
        var max:Int = 0
        
        var tempArray:[UInt16] = [UInt16](count: 160 * 120, repeatedValue: 0)
        var rawImage:[UInt8] = [UInt8](count: 160 * 120, repeatedValue: 0)
        
        for i in 0 ..< 160 * 120 {
            tempArray[i] = rawVideoArray[i]
            
            if (Int(tempArray[i]) > max) {
                max = Int(tempArray[i])
            }
            
            if Int(tempArray[i]) < min {
                min = Int(tempArray[i])
            }
        }
        
        //print(rawVideoArray)
        print(min)
        print(max)
        
        for j in 0 ..< 160 * 120 {
            rawImage[j] = UInt8((Int(tempArray[j]) - min) * 255 / (max - min))
        }
        
        return rawImage
    }
    
    func runSockets() {
        connect("192.168.1.1", port: 5004)
    }
    
    var inputStream: NSInputStream?
    var outputStream: NSOutputStream?
    
//    func connect(host: String, port: Int) {
//        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.rawValue), 0)) { // 1
//            NSStream.getStreamsToHostWithName(host, port: port, inputStream: &self.inputStream, outputStream: &self.outputStream)
//            
//            if self.inputStream != nil && self.outputStream != nil {
//                // Set delegate
//                self.inputStream!.delegate = self
//                self.outputStream!.delegate = self
//                
//                if (self.inputStream?.delegate == nil) {
//                    let viewAction = UIAlertAction(title: "Try again", style: UIAlertActionStyle.Destructive, handler: { (alert: UIAlertAction) in
//                        self.connect("192.168.1.1", port: 5004)
//                    })
//                    
//                    let controller = UIAlertController(title: "Error", message: "No connected device", preferredStyle: UIAlertControllerStyle.Alert)
//                    controller.addAction(viewAction)
//                    self.presentViewController(controller, animated: true, completion: nil)
//                    
//                   return
//                }
//                
//                // Schedule
//                self.inputStream!.scheduleInRunLoop(.mainRunLoop(), forMode: NSDefaultRunLoopMode)
//                self.outputStream!.scheduleInRunLoop(.mainRunLoop(), forMode: NSDefaultRunLoopMode)
//                
//                print("Start open()")
//                
//                self.checkConnectionToCam()
//                
//                // Open!
//                self.inputStream!.open()
//                self.outputStream!.open()
//            
//                let bufferSize = 1400
//                let frameBufferSize = 60 * 80 * 2
//                let frameArrayDataSize = 60 * 80
//                
//                var buffer = Array<UInt8>(count: bufferSize, repeatedValue: 0)
//                var byteRxBuf = Array<UInt8>(count: frameBufferSize, repeatedValue: 0)
//                var frameRawDataArray = Array<UInt16>(count: frameArrayDataSize, repeatedValue: 0)
//                
//                var endFrame: UInt16 = 0
//                //var seqNum: UInt16 = 0
//                var lenData: Int = 0
//                var offset: Int = 0
//                
//                while true {
//                    self.inputStream!.read(&buffer, maxLength: bufferSize)
//                    
//                    // Parse TCP packet
//                    let endFrameAsByte:[UInt8] = [buffer[0], buffer[1]]
//                    endFrame = self.bytesToUInt2(endFrameAsByte)
//                    
//                    let seqNumAsByte:[UInt8] = [buffer[2], buffer[3]]
//                    //seqNum = self.bytesToUInt2(seqNumAsByte)
//                    
//                    let lenDataAsByte:[UInt8] = [buffer[4], buffer[5], buffer[6], buffer[7]]
//                    lenData = Int(self.bytesToUInt(lenDataAsByte))
//                    
//                    // payload started at 8 byte
//                    for i in 0 ..< lenData {
//                        byteRxBuf[offset + i] = buffer[i + 8]
//                    }
//                    
//                    offset += lenData
//                    
//                    if (endFrame == 1) {
//                        offset = 0
//                        
//                        // Convert Byte[] received buff to Short[]
//                        for j in 0 ..< frameArrayDataSize {
//                            let firstByte = byteRxBuf[j * 2]
//                            let secondByte = byteRxBuf[j * 2 + 1]
//                            let pixelDataAsShort = self.bytesToUInt2([secondByte, firstByte])
//                            
//                            frameRawDataArray[j] = UInt16(pixelDataAsShort)
//                        }
//                        
//                        dispatch_async(dispatch_get_main_queue()) {
//                            self.isConnected = true
//                            self.setImageFromRawFrame(frameRawDataArray)
//                        }
//                    }
//                }
//                //            } else {
//                //                // Handle error
//                //            }
//            }
//        }
//    }
    
    func connect(host: String, port: Int) {
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.rawValue), 0)) { // 1
            NSStream.getStreamsToHostWithName(host, port: port, inputStream: &self.inputStream, outputStream: &self.outputStream)
            
            if self.inputStream != nil && self.outputStream != nil {
                // Set delegate
                self.inputStream!.delegate = self
                self.outputStream!.delegate = self
                
                if (self.inputStream?.delegate == nil) {
                    let viewAction = UIAlertAction(title: "Try again", style: UIAlertActionStyle.Destructive, handler: { (alert: UIAlertAction) in
                        self.connect("192.168.1.1", port: 5004)
                    })
                    
                    let controller = UIAlertController(title: "Error", message: "No connected device", preferredStyle: UIAlertControllerStyle.Alert)
                    controller.addAction(viewAction)
                    self.presentViewController(controller, animated: true, completion: nil)
                    
                    return
                }
                
                // Schedule
                self.inputStream!.scheduleInRunLoop(.mainRunLoop(), forMode: NSDefaultRunLoopMode)
                self.outputStream!.scheduleInRunLoop(.mainRunLoop(), forMode: NSDefaultRunLoopMode)
                
                print("Start open()")
                
                self.checkConnectionToCam()
                
                // Open!
                self.inputStream!.open()
                self.outputStream!.open()
                
                let bufferSize = 1400
                let frameBufferSize = 160 * 120 * 2
                let frameArrayDataSize = 160 * 120
                
                var buffer = Array<UInt8>(count: bufferSize, repeatedValue: 0)
                var byteRxBuf = Array<UInt8>(count: frameBufferSize, repeatedValue: 0)
                var frameRawDataArray = Array<UInt16>(count: frameArrayDataSize, repeatedValue: 0)
                
                var endFrame: UInt16 = 0
                var segment: Int = 0
                var lenData: Int = 0
                var offset: Int = 0
                var width: Int = 0
                var height: Int = 0
                
                while true {
                    self.inputStream!.read(&buffer, maxLength: bufferSize)
                    
                    // Parse TCP packet
                    let endFrameAsByte:[UInt8] = [buffer[0], buffer[1]]
                    endFrame = self.bytesToUInt2(endFrameAsByte)
                    
                    let offsetAsByte :[UInt8] = [buffer[2], buffer[3]]
                    offset = Int(self.bytesToUInt2(offsetAsByte))
                    
                    let lenDataAsByte :[UInt8] = [buffer[4], buffer[5]]
                    lenData = Int(self.bytesToUInt2(lenDataAsByte))
                    
                    let segmentAsByte :[UInt8] = [buffer[6], buffer[7]]
                    segment = Int(self.bytesToUInt2(segmentAsByte))
                    
                    let widthAsByte :[UInt8] = [buffer[8], buffer[9]]
                    width = Int(self.bytesToUInt2(widthAsByte))
                    
                    let heightAsByte :[UInt8] = [buffer[10], buffer[11]]
                    height = Int(self.bytesToUInt2(heightAsByte))
                    
                    
//                    print("endFrame: " + String(endFrame))
//                    print("offset: " + String(offset))
//                    print("lenData: " + String(lenData))
//                    print("segment: " + String(segment))
//                    print("width: " + String(width))
//                    print("height: " + String(height))
                
                    
                    
                    if width == 160 && height == 120 {
                        offset = offset + segment * 9600
                    } else if width == 80 && height == 60 {
                        //offset = offset
                    }
                    
                    // payload started at 12 byte
                    for i in 0 ..< lenData {
                        byteRxBuf[offset + i] = buffer[i + 12]
                    }
                    
                    if (endFrame == 1 && segment == 3) {
                        // Convert Byte[] received buff to Short[]
                        for j in 0 ..< frameArrayDataSize {
                            let firstByte : UInt8 = byteRxBuf[j * 2]
                            let secondByte : UInt8 = byteRxBuf[j * 2 + 1]
                            let pixelDataAsShort : UInt16 = self.bytesToUInt2([secondByte, firstByte])
                            
                            frameRawDataArray[j] = pixelDataAsShort
                        }
                        
                        dispatch_async(dispatch_get_main_queue()) {
                            self.isConnected = true
                            self.setImageFromRawFrame(frameRawDataArray)
                        }
                    }
                }
            }
        }
    }
    
    var isConnected : Bool = false
    func checkConnectionToCam() {
        self.isConnected = false
        
        delay(3) {
            if self.isConnected {
                return
            } else {
                let viewAction = UIAlertAction(title: "Try again", style: UIAlertActionStyle.Destructive, handler: { (alert: UIAlertAction) in
                    self.connect("192.168.1.1", port: 5004)
                })
                
                let controller = UIAlertController(title: "Error", message: "No connected device", preferredStyle: UIAlertControllerStyle.Alert)
                controller.addAction(viewAction)
                self.presentViewController(controller, animated: true, completion: nil)
            }
        }
    }
    
    func stream(aStream: NSStream, handleEvent eventCode: NSStreamEvent) {
        if aStream === inputStream {
            switch eventCode {
            case NSStreamEvent.ErrorOccurred:
                print("input: ErrorOccurred: \(aStream.streamError?.description)")
                
                let viewAction = UIAlertAction(title: "Try again", style: UIAlertActionStyle.Destructive, handler: { (alert: UIAlertAction) in
                    self.connect("192.168.1.1", port: 5004)
                })
                
                let controller = UIAlertController(title: "Error", message: "No connected device", preferredStyle: UIAlertControllerStyle.Alert)
                controller.addAction(viewAction)
                self.presentViewController(controller, animated: true, completion: nil)
            case NSStreamEvent.OpenCompleted:
                print("input: OpenCompleted")
            case NSStreamEvent.HasBytesAvailable: break
                //print("input: HasBytesAvailable")
                
                // Here you can `read()` from `inputStream`
                
            default:
                break
            }
        }
        else if aStream === outputStream {
            switch eventCode {
            case NSStreamEvent.ErrorOccurred:
                print("output: ErrorOccurred: \(aStream.streamError?.description)")
            case NSStreamEvent.OpenCompleted:
                print("output: OpenCompleted")
            case NSStreamEvent.HasSpaceAvailable:
                print("output: HasSpaceAvailable")
                
                // Here you can write() to `outputStream`
                
            default:
                break
            }
        }
    }
    
    func bytesToUInt(byteArray: [UInt8]) -> UInt {
        assert(byteArray.count <= 4)
        
        var result: UInt = 0
        for idx in 0..<(byteArray.count) {
            let shiftAmount = UInt((byteArray.count) - idx - 1) * 8
            
            result += UInt(byteArray[idx]) << shiftAmount
        }
        
        return result
    }
    
    func bytesToUInt2(byteArray: [UInt8]) -> UInt16 {
        assert(byteArray.count <= 2)
        
        var result: UInt16 = 0
        
        for idx in 0..<(byteArray.count) {
            let shiftAmount = UInt16((byteArray.count) - idx - 1) * 8
            
            result += UInt16(byteArray[idx]) << shiftAmount
        }
        
        return result
    }
    
    func saveImageToGallery() {
        CustomPhotoAlbum.sharedInstance.saveImage(self.fakeCameraView.image!)
        
        //        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
        //            var imagePath = "images/aim/" + self.getCurrentDateTime() + "_aim.jpg"
        //
        //            //let getImage =  UIImage(data: NSData(contentsOfURL: NSURL(string: self.fakeCameraView)))
        //            UIImageJPEGRepresentation(self.fakeCameraView.image, 100).writeToFile(imagePath, atomically: true)
        //
        ////            dispatch_async(dispatch_get_main_queue()) {
        ////                self.image?.image = getImage
        ////                return
        ////            }
        //        }
    }
    
    func getCurrentDateTime() -> String {
        let date = NSDate()
        let formatter = NSDateFormatter()
        formatter.timeStyle = .ShortStyle
        return formatter.stringFromDate(date)
    }
    
    func createVideo() {
        let frames: NSMutableArray = []
        
        let icon1: UIImage = UIImage(named: "btn_take-photo")!
        let icon2: UIImage = UIImage(named: "btn_photo-mode")!
        let icon3: UIImage = UIImage(named: "Radio-btn_normal")!
        
        let set = CEMovieMaker.videoSettingsWithCodec(AVVideoCodecH264, withWidth: 192, andHeight: 240)
        let movieMaker = CEMovieMaker(settings: set)
        
        for _ in 0...9 {
            frames.addObject(icon1)
            frames.addObject(icon2)
            frames.addObject(icon3)
        }
        
        movieMaker.createMovieFromImages(frames.copy() as! [AnyObject], withCompletion: { (fileURL) -> Void in
            self.saveToCameraRoll(fileURL)
        })
    }
    
    func viewMovieAtUrl(fileURL: NSURL) {
        _ = MPMoviePlayerController(contentURL: fileURL)
    }
    
    func saveToCameraRoll(URL: NSURL!) {
        NSLog("srcURL: %@", URL)
        let library: ALAssetsLibrary = ALAssetsLibrary()
        let videoWriteCompletionBlock: ALAssetsLibraryWriteVideoCompletionBlock = {(newURL: NSURL!, error: NSError!) in
            if (error != nil) {
                NSLog("Error writing image with metadata to Photo Library: %@", error)
            }
            else {
                NSLog("Wrote image with metadata to Photo Library %@", newURL.absoluteString)
            }
            
        }
        if library.videoAtPathIsCompatibleWithSavedPhotosAlbum(URL) {
            library.writeVideoAtPathToSavedPhotosAlbum(URL, completionBlock: videoWriteCompletionBlock)
        }
    }
    
    func RBResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSizeMake(size.width * heightRatio, size.height * heightRatio)
        } else {
            newSize = CGSizeMake(size.width * widthRatio,  size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.drawInRect(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    //Camera functions
    
    func configureDevice() {
        if let device = captureDevice {
            do {
                try device.lockForConfiguration()
            } catch _ {
            }
            device.focusMode = .Locked
            device.unlockForConfiguration()
        }
    }
    
    func beginSession() {
        configureDevice()
        
        let err : NSError? = nil
        do {
            try captureSession.addInput(AVCaptureDeviceInput(device: captureDevice))
        } catch {
            
        }
        
        if err != nil {
            print("error: \(err?.localizedDescription)")
        }
        
        //previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        //fakeCameraView.layer.addSublayer(previewLayer)
        //previewLayer?.frame = fakeCameraView.layer.frame
        //previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        captureSession.startRunning()
        
        
        addImagePreview()
    }
    
    func addImagePreview() {
        stillImageOutput.outputSettings = [AVVideoCodecKey:AVVideoCodecJPEG]
        if captureSession.canAddOutput(stillImageOutput) {
            captureSession.addOutput(stillImageOutput)
        }
        
    }
    
    var soundID: SystemSoundID = 0
    
    func captureImageFromCameraPreview() {
        if let videoConnection = stillImageOutput.connectionWithMediaType(AVMediaTypeVideo) {
            if (soundID == 0) {
                let filePath = NSBundle.mainBundle().pathForResource("photoShutter2", ofType:"caf")
                let alertSound = NSURL(fileURLWithPath: filePath!)
                AudioServicesCreateSystemSoundID(alertSound, &soundID);
            }
            
            AudioServicesPlaySystemSound(soundID);
            
            stillImageOutput.captureStillImageAsynchronouslyFromConnection(videoConnection) {
                (imageDataSampleBuffer, error) -> Void in
                
                
                //AudioServicesPlaySystemSound(self.soundID);
                
                let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)
                let image = UIImage(data: imageData)
                
                self.changeImage(image!)
                //self.imagePreview.image = image
            }
        }
    }
    
    @IBOutlet weak var imagePreview: UIImageView!
    
    func changeImage(image: UIImage) {
        let size = CGSize(width: 192, height: 240)
        self.imagePreview.image = RBResizeImage(image, targetSize: size).getAlphaCounture()
    }
    
    var sessionQueue: dispatch_queue_t!
    func initializeAndStartSessionWithoutCaptureSound_1()
    {
        sessionQueue = dispatch_queue_create("cameraQueue", DISPATCH_QUEUE_SERIAL)
        
        var captureInput: AVCaptureDeviceInput
        do {
            captureInput = try AVCaptureDeviceInput(device: captureDevice)
        } catch {
            print("")
            return
            //err = error
            //captureInput = nil
        }
        
        let captureOutput: AVCaptureVideoDataOutput = AVCaptureVideoDataOutput()
        
        captureOutput.alwaysDiscardsLateVideoFrames = true
        
        let key: NSString = kCVPixelBufferPixelFormatTypeKey;
        let videoSettings = NSDictionary(object: Int(kCVPixelFormatType_32BGRA), forKey:  key)
        
        captureOutput.videoSettings = videoSettings as [NSObject : AnyObject]
        captureOutput.setSampleBufferDelegate(self, queue: sessionQueue)
        
        captureSession.addInput(captureInput)
        captureSession.addOutput(captureOutput)
        
        captureSession.startRunning()
    }
    
    /* AVCaptureVideoDataOutput Delegate
    ------------------------------------------*/
    
    var sessionDelegate: CameraSessionControllerDelegate?
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, fromConnection connection: AVCaptureConnection!) {
        //////////////
        /*We create an autorelease pool because as we are not in the main_queue our code is
        not executed in the main thread. So we have to create an autorelease pool for the thread we are in*/
        
        autoreleasepool {
            if (cameraPalette == AimFrameCreator.FUSION_PALLETE) {
                let imageBuffer:CVImageBufferRef = CMSampleBufferGetImageBuffer(sampleBuffer)!
                CVPixelBufferLockBaseAddress(imageBuffer, 0)
                
                let baseAddress = UnsafeMutablePointer<UInt8>(CVPixelBufferGetBaseAddress(imageBuffer))
                
                let bytesPerRow: size_t = CVPixelBufferGetBytesPerRow(imageBuffer)
                let width: size_t = CVPixelBufferGetWidth(imageBuffer)
                let height: size_t = CVPixelBufferGetHeight(imageBuffer)
                
                let colorSpace = CGColorSpaceCreateDeviceRGB()
                let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.PremultipliedFirst.rawValue)
                
                let newContext = CGBitmapContextCreate(baseAddress, width, height, 8,  bytesPerRow, colorSpace, bitmapInfo.rawValue)
                let newImage: CGImageRef = CGBitmapContextCreateImage(newContext)!
                
                var finalImageFromCamera = UIImage(CGImage: newImage, scale: 1.0, orientation: UIImageOrientation.Right)
                finalImageFromCamera = RBResizeImage(finalImageFromCamera, targetSize: CGSize(width: 240, height: 320))
                let stitchedImage:UIImage = CVWrapper.imageCounture(finalImageFromCamera) as UIImage
                
                
                dispatch_async(dispatch_get_main_queue(),{
                    
                    self.imagePreview.image = stitchedImage
                })
            }
        }
        
        sessionDelegate?.cameraSessionDidOutputSampleBuffer?(sampleBuffer)
    }
    
    func downloadNewModuleProgramm() {
        ProgressView.shared.showProgressView(view)
        
        delay(2.0) {
            ProgressView.shared.hideProgressView()
            
            let alert = UIAlertController(title: "Обновление", message: "Найдена новая версия прошивки тепловизора. Обновить?", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Да", style: .Default, handler: { action in
                ProgressView.shared.showProgressView(self.view)
                
                self.delay(10.0) {
                    ProgressView.shared.hideProgressView()
                    
                    let alert = UIAlertController(title: "Обновление", message: "Прошивка успешно обновлена", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }))
            alert.addAction(UIAlertAction(title: "Нет", style: .Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
}