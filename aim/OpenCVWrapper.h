//
//  OpenCVWrapper.h
//  aim
//
//  Created by Артем Селюжицкий on 05.09.15.
//  Copyright (c) 2015 traz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface OpenCVWrapper : NSObject

+ (UIImage *)processImageWithOpenCV:(UIImage*)inputImage;

@end