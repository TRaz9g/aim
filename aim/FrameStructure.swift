//
//  FrameStructure.swift
//  aim
//
//  Created by Артем Селюжицкий on 22.07.15.
//  Copyright (c) 2015 traz. All rights reserved.
//

import Foundation

public struct FrameStructure {
    var a: UInt8
    var r: UInt8
    var g: UInt8
    var b: UInt8
}