//
//  Connection.swift
//  aim
//
//  Created by Артем Селюжицкий on 22.07.15.
//  Copyright (c) 2015 traz. All rights reserved.
//

import Foundation

class Connection: NSObject, NSStreamDelegate {
    var host:String?
    var port:Int?
    var inputStream: NSInputStream?
    var outputStream: NSOutputStream?
    
    func connect(host: String, port: Int) {
        
        self.host = host
        self.port = port
        
        NSStream.getStreamsToHostWithName(host, port: port, inputStream: &inputStream, outputStream: &outputStream)
        
        if inputStream != nil && outputStream != nil {
            
            // Set delegate
            inputStream!.delegate = self
            outputStream!.delegate = self
            
            // Schedule
            inputStream!.scheduleInRunLoop(.mainRunLoop(), forMode: NSDefaultRunLoopMode)
            outputStream!.scheduleInRunLoop(.mainRunLoop(), forMode: NSDefaultRunLoopMode)
            
            print("Start open()")
            
            // Open!
            inputStream!.open()
            outputStream!.open()
            
            
            let bufferSize = 1400
            let frameBufferSize = 60 * 80 * 2
            let frameArrayDataSize = 60 * 80
            
            var buffer = Array<UInt8>(count: bufferSize, repeatedValue: 0)
            var byteRxBuf = Array<UInt8>(count: frameBufferSize, repeatedValue: 0)
            var frameRawDataArray = Array<UInt16>(count: frameArrayDataSize, repeatedValue: 0)
            
            var endFrame: UInt = 0
            var seqNum: UInt = 0
            var lenData: Int = 0
            var offset: Int = 0
            
            //let bytesRead = inputStream!.read(&buffer, maxLength: bufferSize)
            
            while true {
                inputStream!.read(&buffer, maxLength: bufferSize)
                
                // Parse TCP packet
                let endFrameAsByte:[UInt8] = [buffer[0], buffer[1]]
                //endFrame = UnsafePointer<UInt16>(endFrameAsByte).memory
                endFrame = bytesToUInt2(endFrameAsByte)
                
                let seqNumAsByte:[UInt8] = [buffer[2], buffer[3]]
                //seqNum = UnsafePointer<UInt16>(endFrameAsByte).memory
                seqNum = bytesToUInt2(seqNumAsByte)
                
                let lenDataAsByte:[UInt8] = [buffer[4], buffer[5], buffer[6], buffer[7]]
                lenData = Int(bytesToUInt(lenDataAsByte))
                
                // payload started at 8 byte
                for i in 8...lenData - 1 {
                    byteRxBuf[offset + i] = buffer[i]
                }
                
                offset += lenData
                
                if (endFrame == 1) {
                    offset = 0
                    
                    // Convert Byte[] received buff to Short[]
                    for j in 0...frameArrayDataSize - 1 {
                        let firstByte = byteRxBuf[j * 2]
                        let secondByte = byteRxBuf[j * 2 + 1]
                        let pixelDataAsShort = bytesToUInt2([firstByte, secondByte])
                        
                        frameRawDataArray[j] = UInt16(pixelDataAsShort)
                    }
                    
                    print(frameRawDataArray)
                }
            }
//            } else {
//                // Handle error
//            }
        }
    }
    
    func bytesToUInt(byteArray: [UInt8]) -> UInt {
        assert(byteArray.count <= 4)
        
        var result: UInt = 0
        for idx in 0..<(byteArray.count) {
            let shiftAmount = UInt((byteArray.count) - idx - 1) * 8
            
            result += UInt(byteArray[idx]) << shiftAmount
        }
        
        return result
    }
    
    func bytesToUInt2(byteArray: [UInt8]) -> UInt {
        assert(byteArray.count <= 2)
        
        var result: UInt = 0
        for idx in 0..<(byteArray.count) {
            let shiftAmount = UInt((byteArray.count) - idx - 1) * 8
            
            result += UInt(byteArray[idx]) << shiftAmount
        }
        
        return result
    }
    
    func stream(aStream: NSStream, handleEvent eventCode: NSStreamEvent) {
        if aStream === inputStream {
            switch eventCode {
            case NSStreamEvent.ErrorOccurred:
                print("input: ErrorOccurred: \(aStream.streamError?.description)")
            case NSStreamEvent.OpenCompleted:
                print("input: OpenCompleted")
            case NSStreamEvent.HasBytesAvailable:
                print("input: HasBytesAvailable")
                
                // Here you can `read()` from `inputStream`
                
            default:
                break
            }
        } else if aStream === outputStream {
            switch eventCode {
            case NSStreamEvent.ErrorOccurred:
                print("output: ErrorOccurred: \(aStream.streamError?.description)")
            case NSStreamEvent.OpenCompleted:
                print("output: OpenCompleted")
            case NSStreamEvent.HasSpaceAvailable:
                print("output: HasSpaceAvailable")
                
                // Here you can write() to `outputStream`
                
                //outputStream!.write("Hello", maxLength: 5)
                
            default:
                break
            }
        }
    }
    
}