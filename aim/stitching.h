//
//  stitching.h
//  aim
//
//  Created by Артем Селюжицкий on 05.09.15.
//  Copyright (c) 2015 traz. All rights reserved.
//

#ifndef CVOpenTemplate_Header_h
#define CVOpenTemplate_Header_h
#include <opencv2/opencv.hpp>

cv::Mat stitch (std::vector <cv::Mat> & images);


#endif