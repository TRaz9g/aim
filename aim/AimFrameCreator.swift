//
//  AimFrameCreator.swift
//  aim
//
//  Created by Артем Селюжицкий on 22.07.15.
//  Copyright (c) 2015 traz. All rights reserved.
//

import Foundation

public class AimFrameCreator {
    
    public static var GREY_PALLETE: String = "grey_pallete"
    public static var INVERTED_GREY_PALLETE: String = "inverted_grey_pallete"
    public static var COLOR_PALLETE: String = "color_pallete"
    public static var FUSION_PALLETE: String = "fusion_pallete"
    
//    public struct PixelData {
//        var a: UInt8
//        var r: UInt8
//        var g: UInt8
//        var b: UInt8
//    }
    
    static func createFrame(videoArray:[UInt8], frameType: String) -> UIImage {
        return UIImage()
    }
    
//    private static func makeFrame(videoArray:[UInt16], frame: Int)->[PixelData] {
//        var data: [PixelData] = []
//        let startPosition = frame * 4800
//        let finalPosition = startPosition + 4800
//        
//        var tempVideoArray:[UInt16] = [UInt16](count: 4800, repeatedValue: 0)
//        
//        for frame in 0...80*60 - 1 {
//            tempVideoArray[frame] = videoArray[startPosition + frame]
//        }
//        
//        let finalVideoFrame : [UInt8] = transformArray(tempVideoArray)
//        
//        for(var y = 0; y < 4800; y++) {
//            data.append(createPixelData(finalVideoFrame[y], framePalleteType: ))
//        }
//        
//        return data
//    }
    
    public static func createPixelData(pixelNumber: Int, framePalleteType: String) -> FrameStructure {
        if (framePalleteType == GREY_PALLETE) {
            return FrameStructure(
                a: 255,
                r: AimDefines.gray[pixelNumber * 3],
                g: AimDefines.gray[pixelNumber * 3 + 1],
                b: AimDefines.gray[pixelNumber * 3 + 2]
            )
        } else if (framePalleteType == INVERTED_GREY_PALLETE) {
            return FrameStructure(
                a: 255,
                r: AimDefines.invGray[pixelNumber * 3],
                g: AimDefines.invGray[pixelNumber * 3 + 1],
                b: AimDefines.invGray[pixelNumber * 3 + 2]
            )
        } else if (framePalleteType == COLOR_PALLETE) {
            return FrameStructure(
                a: 255,
                r: AimDefines.rainbow[pixelNumber * 3],
                g: AimDefines.rainbow[pixelNumber * 3 + 1],
                b: AimDefines.rainbow[pixelNumber * 3 + 2]
            )
        } else if (framePalleteType == FUSION_PALLETE) {
            return FrameStructure(
                a: 255,
                r: AimDefines.ironbow[pixelNumber * 3],
                g: AimDefines.ironbow[pixelNumber * 3 + 1],
                b: AimDefines.ironbow[pixelNumber * 3 + 2]
            )
        }
        
        return FrameStructure(
            a: 255,
            r: AimDefines.gray[pixelNumber],
            g: AimDefines.gray[pixelNumber + 1],
            b: AimDefines.gray[pixelNumber + 2]
        )
    }
    
    private static func transformArray(rawVideoArray: [UInt16]) -> [UInt8] {
        var min:Int = 1 << 15
        var max:Int = 0
        
        var tempArray:[UInt16] = [UInt16](count: 160 * 120, repeatedValue: 0)
        var rawImage:[UInt8] = [UInt8](count: 160 * 120, repeatedValue: 0)
        
        for i in 0 ..< 160 * 120 {
            tempArray[i] = rawVideoArray[i]
            
            if (Int(tempArray[i]) > max) {
                max = Int(tempArray[i])
            }
            
            if (Int(tempArray[i]) < min) {
                min = Int(tempArray[i])
            }
        }
        
        for j in 0 ..< 160 * 120 {
            rawImage[j] = UInt8((Int(tempArray[j]) - min) * 255 / (max - min))
        }
        
        return rawImage
    }
    
    private static func imageFromARGB32Bitmap(pixels: [FrameStructure], width: Int, height: Int) -> UIImage? {
        let bitsPerComponent: Int = 8
        let bitsPerPixel: Int = 32
        let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo:CGBitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.PremultipliedFirst.rawValue)
        
        var data = pixels
        
        let providerRef = CGDataProviderCreateWithCFData(NSData(bytes: &data, length: data.count * sizeof(FrameStructure)))
        //let providerRefthing: CGDataProvider = providerRef!
        
        let cgim = CGImageCreate(
            width,
            height,
            bitsPerComponent,
            bitsPerPixel,
            width * Int(sizeof(FrameStructure)),
            rgbColorSpace,
            bitmapInfo,
            providerRef,
            nil,
            true,
            CGColorRenderingIntent.RenderingIntentDefault
        )
        
        let portraitImage = UIImage(CGImage: cgim!, scale: 1.0, orientation: UIImageOrientation.Right)
        
        return portraitImage
    }
}